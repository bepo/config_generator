```
Usage: kcg [OPTIONS] COMMAND [ARGS]...

  The command line of KCG: the Keyboard Configuration Generator.

Options:
  --help  Show this message and exit.

Commands:
  generate  Generate configuration files for a given target.
```

## Command `generate`

```
Usage: kcg generate [OPTIONS] CONTEXT COMMAND [ARGS]...

  Generate configuration files for a given target.

  CONTEXT_FILE: Path to the context file. Run 'kcg help context' to know more.

Options:
  -o, --out DIRECTORY  Directory where to write output files. By default:
                       working directory.
  --help               Show this message and exit.

Commands:
  doc         Generate documentation files.
  kbdmap      Generate the key kbdmap file.
  keymaps     Generate the key map file.
  klavaro     Generate the config files for Klavaro.
  ktouch      Generate the config files for ktouch.
  mac         Generate config files for Mac.
  map         Generate the layout map in a text file.
  svg         Generate a picture of the layout in a svg file.
  typefaster  Generate the config files for typefaster.
  win         Generate config files for Windows.
  wscons      Generate the wscons file.
  x           Generate config files for Linux X service.
```

## Python docs

::: kcg.cli
