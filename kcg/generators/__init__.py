"""This package contains module to generate output files.

Each module from this package is dedicated to a specific target,
and contains a method `generate`.
"""

from . import doc, win, x
