# coding: utf-8
"""Generators for documentation."""
import pathlib

from kcg.generators.perl import run_perl_script


def generate(context: dict, output: pathlib.Path) -> None:
    """Generate documentation files.

    Args:
        context (dict): the context as a dict (read doc/context.md).
        output (pathlib.Path): directory to write output files.
    """
    name = context["basic_info"]["name"]
    run_perl_script(context, "description", output.joinpath(f"{name}-desc.html"))
