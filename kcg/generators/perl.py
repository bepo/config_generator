# coding: utf-8
"""This module is used to make a bridge with the Perl script."""

# TODO: do not use the PERL script anymore, but only python and Jinja.

import logging
import os
import pathlib
import subprocess

from kcg.exceptions import GenerationError

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))


def run_perl_script(
    context: dict,
    perl_function: str,
    output_file: pathlib.Path,
    encoding: str = "utf-8",
) -> None:
    """Run the Perl script to generate files.

    This is to used the legacy code, but this function should be removed in the future
    and the Perl script replaced by 100% Python.
    """
    # Create environment variables for the Perl script
    env = dict(os.environ).update(
        {
            "BEPO_VERSION": context["basic_info"]["version"],
            "BEPO_LAYOUT_DESCRIPTION": context["files"]["layout"],
            "BEPO_DEADKEY_BEHAVIOUR": context["files"]["deadkeys"],
            "BEPO_DOUBLE_DEADKEY_BEHAVIOUR": context["files"]["double_deadkeys"],
            "BEPO_VIRTUAL_KEYS": context["files"]["virtual_keys"],
            "BEPO_KEYS_FILE": context["files"]["keys"],
            "BEPO_SPECIAL_KEYS_FILE": context["files"]["special_keys"],
            "BEPO_SYMBOLS_FILE": context["files"]["symbols"],
            "BEPO_UNICODE_FILE": context["files"]["unicode"],
        }
    )
    perl_command = ["perl", "./configGenerator.pl", perl_function]
    with subprocess.Popen(
        perl_command,
        env=env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    ) as proc:
        if stderr := proc.stderr.read():
            logging.error(stderr)
        try:
            with open(output_file, "w", encoding=encoding) as fpointer:
                fpointer.write(proc.stdout.read())
        except UnicodeDecodeError as exc:
            raise GenerationError(f"Error while generating {output_file}") from exc
