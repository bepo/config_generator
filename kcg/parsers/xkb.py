# coding: utf-8
"""Module to parse XKB files.

It uses the system library libxkbcommon.
"""
from pathlib import Path
from xkbcommon import xkb


def parse(filename: Path):
    """Parse a .xkb file.

    Args:
        filename (pathlib.Path): path to local file to parse.

    Returns:
        (xkbcommon.xkb.Keymap) A xkbcommon keymap object.
    """
    context = xkb.Context()
    with open(filename, "r+") as fpt:
        # open with r+ because keymap_new_from_file uses mmap, which tries
        # to map read/write methods on the file. But do not worry, file is not
        # modified whatsoever.
        keymap = context.keymap_new_from_file(fpt)
    return keymap
