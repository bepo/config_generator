"""This package contains modules to parse specific files.

Each module is dedicated to a type of file, and contains a method `parse` which can be
called to read a file on disk and return its parsed content. Returned object depends on
the module.
"""
