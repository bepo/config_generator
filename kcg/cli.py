# coding: utf-8
"""This module configures the command line of KCG.

Warning: in this file, the word 'context' is used with two different meanings:
    - one is the context object of Click, stored in variable 'ctx'.
      Read https://click.palletsprojects.com/en/8.1.x/api/#click.Context
    - the other is the context file of KCG (this tool), which is a INI file.
      Its documentation is in doc/context.md
"""

import os
import pathlib

import click

from kcg import generators
from kcg.context import ContextType
from kcg.exceptions import GenerationError

DOC_PATH = pathlib.Path(os.path.dirname(os.path.realpath(__file__))).parent.joinpath(
    "doc"
)


@click.group()
def kcg_main_cli():
    """The command line of KCG: the Keyboard Configuration Generator."""


@kcg_main_cli.group()
@click.pass_context
@click.argument(
    "context",
    type=ContextType(),
)
@click.option(
    "-o",
    "--out",
    "output_dir",
    type=click.Path(
        exists=True, file_okay=False, writable=True, path_type=pathlib.Path
    ),
    help="Directory where to write output files. By default: working directory.",
)
def generate(ctx, context, output_dir):
    """Generate configuration files for a given target.

    CONTEXT_FILE: Path to the context file. Read documentation to know more.
    """
    ctx.ensure_object(dict)
    ctx.obj["context"] = context
    ctx.obj["output_dir"] = output_dir


@generate.command()
@click.pass_context
def doc(ctx):
    """Generate documentation files."""
    try:
        generators.doc.generate(ctx.obj["context"], ctx.obj["output_dir"])
    except GenerationError as exc:
        raise click.ClickException(exc)


@generate.command()
@click.pass_context
@click.option(
    "-t",
    "--target",
    type=click.Choice(["xkb-root", "xkb-user", "xmodmap", "xcompose"]),
    help="A single target (file) to generate. By default, every files are generated.",
)
def x(ctx, target):  # pylint: disable=C0103
    """Generate config files for Linux X service."""
    try:
        generators.x.generate(ctx.obj["context"], ctx.obj["output_dir"], target)
    except GenerationError as exc:
        raise click.ClickException(exc)


@generate.command()
@click.pass_context
@click.option(
    "-t",
    "--target",
    type=click.Choice(["azerty", "bépo", "qwertz"]),
    help="A single target (file) to generate. By default, every files are generated.",
)
def win(ctx, target):
    """Generate config files for Windows."""
    try:
        generators.win.generate(ctx.obj["context"], ctx.obj["output_dir"], target)
    except GenerationError as exc:
        raise click.ClickException(exc)


@generate.command()
def mac():
    """Generate config files for Mac."""
    raise NotImplementedError


@generate.command(name="map")
def _map():
    """Generate the layout map in a text file."""
    raise NotImplementedError


@generate.command()
def svg():
    """Generate a picture of the layout in a svg file."""
    raise NotImplementedError


@generate.command()
def klavaro():
    """Generate the config files for Klavaro."""
    raise NotImplementedError


@generate.command()
def ktouch():
    """Generate the config files for ktouch."""
    raise NotImplementedError


@generate.command()
def typefaster():
    """Generate the config files for typefaster."""
    raise NotImplementedError


@generate.command()
def keymaps():
    """Generate the key map file."""
    raise NotImplementedError


@generate.command()
def kbdmap():
    """Generate the key kbdmap file."""
    raise NotImplementedError


@generate.command()
def wscons():
    """Generate the wscons file."""
    raise NotImplementedError
