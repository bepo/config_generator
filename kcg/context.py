# coding: utf-8
"""This module contains functions relative to the context file and content."""

import configparser
import os
import typing

import click


def read_context(context_file: typing.TextIO) -> configparser.ConfigParser:
    """Parse the context file.

    Args:
        context_file (file-like object): the context file.

    Returns:
        ConfigParser instance.
    """
    config = configparser.ConfigParser()
    config.read_file(context_file)
    return config


class ContextType(click.ParamType):
    name = "context"

    def convert(self, value, param, ctx):
        if not os.path.isfile(value):
            self.fail(f"{value!r} is not a valid file path.", param, ctx)

        try:
            with open(value) as fpointer:
                return dict(read_context(fpointer))
        except OSError as exc:
            self.fail(f"Cannot open context file {value!r}: {exc}", param, ctx)
        except configparser.Error as exc:
            self.fail(f"Cannot parse context file {value!r}: {exc}", param, ctx)
