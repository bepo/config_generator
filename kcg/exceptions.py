# coding: utf-8


class GenerationError(RuntimeError):
    """Exception raised when an error occured while generating a file."""
