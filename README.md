# KCG: the Keyboard Configuration Generator

This is a tool made by the Ergodis community to generate configuration files for
its “Bépo” keyboard layout.

Documentation is available on
[bepo.gitlab.io/config_generator](https://bepo.gitlab.io/config_generator).

## Installation

Before installing the Python package, a system library must be installed :
`libxkbcommon-dev` in order to install Python package `xkbcommon`.

```sh
apt-get install libxkbcommon-dev
```

This is a standard Python package. You can install it from pypi with:

```sh
pip install kcg
```

Or you can clone this repository locally and install it from the sources.

This repository is using [poetry](https://python-poetry.org/) to build the
package, so you can run these commands in the cloned repository:

```sh
# poetry creates a new virtual environment for kcg
poetry install
# poetry lets you run kcg in the virtual env
poetry run kcg
```

## Usage

Once installed, the command line `kcg` is available. Use `--help` to get
help in your terminal:

```sh
$ kcg --help
```

To generate configuration files, you need to create a “context” file containing
information about your keyboard and the location of its source files. Please
read
[docs/configuration-files/context.md](https://gitlab.com/bepo/config_generator/-/tree/main/docs/configuration-files/context.md)
to get more details.

Then you can use the subcommand `generate` to generate some configuration files
for a given target. For instance, to generate keyboard files for the Linux X
system, run:

```sh
$ kcg generate /path/to/context.ini --output /output/path x
```

Do no hesitate to use the `--help` option to get more details about
subcommands.

## Contributing

Please do merge requests or open issues on
[the Gitlab project](https://gitlab.com/bepo/config_generator).

### Git workflow

The `main` branch is the one used for releases, and should always be stable.
Contributions should be made in branches or forks, and merged on `main` using
Merge Requests.

Each release is assigned to a tag. Python packages are deployed to Pypi for
each new release.

### Coding style

Please use [pre-commit](https://pre-commit.com/) when coding on this
repository. It should ensure some rules are observed.

[Black](https://github.com/psf/black) is used for formatting.

Docstrings are written using
[Google style](https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings).
